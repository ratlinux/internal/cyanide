cfdisk yourDisk (sfdisk yourDisk < diskLayout > sfdisk_log.txt)

mkfs.vfat yourDisk

mkfs.ext4 yourDisk

mount yourDisk1 /mnt; mount yourDisk2 /mnt/boot --mkdir

reflector -c yourCountry > /etc/pacman.d/mirrorlist

cp -f pacman.conf /etc/pacman.conf

pacstrap -K /mnt \ 
# System
base \ 
linux-zen \ # Kernel 
linux-firmware \ 
base-devel \  
pipewire \ # Pipewire
pipewire-pulse \ 
pipewire-alsa \ 
pipewire-jack \ 
wireplumber \ 
networkmanager \ # Network
archlinux-keyring \ 
systemd-sysvcompat \ 
sudo \ 
# Desktop
xorg \ 
plasma \  
kde-utilities \ 
kde-system \ 
sddm \ 
dolphin \ # File Manager
konsole \ # Terminal Emulator
discover \ # Application Store
breeze-gtk \ 
# Fonts 
noto-fonts \ 
noto-fonts-emoji \ # Emoji Font
noto-fonts-cjk \ 
noto-fonts-extra \ 
ttf-nerd-fonts-symbols-common \ 
# Optional Packages
neofetch \ 
curl \ 
nano \ 
vim \ 
man-db \ 
man-pages 

