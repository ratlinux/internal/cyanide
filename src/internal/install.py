import os

defaultPkgs = [
# System
"base",
"linux-zen", # Kernel 
"linux-firmware", 
"base-devel",  
"pipewire", # Pipewire
"pipewire-pulse", 
"pipewire-alsa", 
"pipewire-jack", 
"wireplumber", 
"networkmanager", # Network
"archlinux-keyring", 
"systemd-sysvcompat", 
"sudo",
# Desktop
"xorg", 
"plasma",  
"kde-utilities", 
"kde-system",
"sddm",
"dolphin", # File Manager
"konsole", # Terminal Emulator
"discover", # Application Store
"breeze-gtk", 
# Fonts 
"noto-fonts",
"noto-fonts-emoji",  # Emoji Font
"noto-fonts-cjk",
"noto-fonts-extra",  
"ttf-nerd-fonts-symbols-common",  
# Optional Packages
"neofetch",
"curl",
"nano", 
"vim",  
"man-db",  
"man-pages",
]   

def install(pkgList=defaultPkgs, mountpoint="/mnt"):
    pkgs = " ".join(pkgList)
    os.system(f"pacstrap -K {mountpoint} " + pkgs)
    return pkgs
    