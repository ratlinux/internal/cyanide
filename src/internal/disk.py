import os

# Example for drive: /dev/sda
def partition(drive, mountpoint='/mnt', fsCmd1="mkfs.vfat", fsCmd2="mkfs.ext4"):   
    os.system("mkdir logs > /dev/null 2>&1")    
    os.system(f"sfdisk {drive} < diskLayout > logs/sfdisk_log.txt 2>&1")
    os.system(f"{fsCmd1} {drive}1 > logs/{fsCmd1}.txt 2>&1")
    os.system(f"{fsCmd2} {drive}2 > logs/{fsCmd2}.txt 2>&1")
    os.system(f"mount {drive}1  {mountpoint}")
    os.system(f"mount {drive}2 {mountpoint}/boot --mkdir")
    return drive