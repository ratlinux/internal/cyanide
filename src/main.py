from internal import install, disk, config
import os

def diskPart():
    os.system("clear")
    os.system("lsblk")
    print("\n!!EVERYTHING ON THE DISK YOU CHOSE WOULD BE WIPED!!\n")
    drive = input("What drive you are going to use? (In example: /dev/sda, /dev/sdb and so on) Use the disk listing on top for refrence >>  ").lower()
    os.system(f"cfdisk {drive}")
    disk.partition(drive)

def setMirrors():
    country = input("Select mirror region (Your country / country code) >>  ").lower()
    config.mirrors(country)

def installSystem():
    os.system("clear")
    i = input("Do you want to proceed with the installation? [y/N] >>  ").lower()
    if i == ("n" or "no"):
        print(i)
        print("Exiting...")
        exit(1)
    elif i == ("y" or "yes"):
        install.install()
        config.genfstab()
        config.network() 

    
if __name__ == "__main__":
    diskPart()
    setMirrors()
    installSystem()
    